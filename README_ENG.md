# VSU POCKET

The purpose of the project was to create a convenient information platform for the Vitebsk State University. Includes: 
- updated timetable
- call information
- canteen menu information


 Technologies used:
- Kotlin
- Google map api
- firebase (notification, analytics, database) 

The project has not been completed yet due to work on a more interesting project, which will be much better and will be cross-platform.

Screenshots: https://imgur.com/7JW8xND
